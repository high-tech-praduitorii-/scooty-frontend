import React, { Component } from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { Login } from './AppScreens/Views/Login';
import {Maps} from './AppScreens/Views/Maps';
import { Register } from './AppScreens/Views/Register';
import { PermissionsAndroid } from 'react-native';
import { ScanQR } from './AppScreens/Views/ScanQR.js';
import firebase from 'react-native-firebase';
import { GenerateQR } from './AppScreens/Views/GenerateQR';
import {Card} from './AppScreens/Views/Card';

const initialRoute = firebase.auth().currentUser !== null ? "MapsRT" : "LoginRT";

const Routes = createStackNavigator({
  LoginRT: Login,
  MapsRT: Maps,
  RegisterRT: Register,
  ScanQR: ScanQR,
  GenerateQRRT: GenerateQR,
  CardRT: Card,
},
{
  initialRouteName: initialRoute,
})

const AppContainer = createAppContainer(Routes);

export default class App extends Component {
  constructor() {
    super();
  }

  // requestLocationPermission = async () => {
  //   const granted = await PermissionsAndroid.request(
  //     PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  //     {
  //       title: 'Avem nevoie de acces la locatie pentru a imbunatati experienta Scooty'
  //   })
  // }

  // componentDidMount() {
  //   this.requestLocationPermission();
  // }


  render() {
    return (
      <AppContainer />
    );
  }
}