import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Maps } from '../Views/Maps.js';

export default class RideView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            timer: null,
            minutes_Counter: '00',
            seconds_Counter: '00',
            lei_Counter: '3',
            bani_Counter: '00',
            rideStarted: false,
            userID: '',
            scooterID: ''
        }
    }

    componentDidMount() {
        this.startRide();
    }

    startRide = () => {
        this.interval = setInterval(() => {
            console.log("One more");

            var num = (Number(this.state.seconds_Counter) + 1).toString(),
                count = this.state.minutes_Counter;
            bani = this.state.bani_Counter;
            lei = this.state.lei_Counter;

            if (Number(this.state.seconds_Counter) == 59) {
                count = (Number(this.state.minutes_Counter) + 1).toString();
                num = '00';
                bani = (Number(this.state.bani_Counter) + 25).toString();
            }

            if (Number(bani) == 100) {
                lei = (Number(this.state.lei_Counter) + 1).toString();
                bani = '00';
            }

            this.setState({
                minutes_Counter: count.length == 1 ? '0' + count : count,
                seconds_Counter: num.length == 1 ? '0' + num : num,
                bani_Counter: bani,
                lei_Counter: lei
            });
        }, 1000);
        this.setState({ timer: this.interval });
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.rideViewContainer}>
                    <View style={styles.statsContainer}>
                        <View style={styles.statsContainer2}>
                            <Text style={styles.statsText}>Timpul cursei :    {this.state.minutes_Counter} : {this.state.seconds_Counter}</Text>
                        </View>
                    </View>
                    <View style={styles.statsContainer}>
                        <View style={styles.statsContainer3}>
                            <Text style={styles.statsText}>Pretul cursei :    {this.state.lei_Counter}.{this.state.bani_Counter} lei</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.endRideContainer}>
                    <TouchableOpacity style={styles.endRideContainer2} onPress={() => this.props.endRide(this.state.minutes_Counter, this.state.seconds_Counter, this.state.lei_Counter, this.state.bani_Counter)}>
                        <Text style={styles.endRideText}>Incheie cursa</Text>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        height: '100%',
        width: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    button: {
        width: 80,
        paddingHorizontal: 12,
        alignItems: "center",
        marginHorizontal: 10
    },
    buttonContainer: {
        flexDirection: "row",
        // marginBottom: 20,
        backgroundColor: "transparent",
        top: '20%'
    },
    bubble: {
        flex: 1,
        backgroundColor: "rgba(32,32,32,0.65)",
        paddingHorizontal: 18,
        paddingVertical: 12,
        borderRadius: 20,
        // bottom: 10
    },
    latlng: {
        width: 200,
        alignItems: "stretch"
    },
    rideView: {
        flex: 1,
        marginTop: 500,
        width: '100%',
        backgroundColor: "#323232",
    },
    headerContainer: {
        flexDirection: "row",
        height: '10%',
        alignItems: "center",
        backgroundColor: "#323232",
    },
    statsText: {
        fontSize: 28,
        textAlign: "center",
        color: "rgb(59,216,13)",
        marginVertical: 5,
    },
    statsTitleContainer: {
        flex: 1,
        alignItems: "center",
        backgroundColor: "#323232",
    },
    statsTitleText: {
        textAlign: "center",
        fontSize: 26,
        color: "rgb(59,216,13)",
    },
    endRideContainer: {
        flex: 1,
        marginHorizontal: 20,
        marginVertical: 10,
        backgroundColor: "#F3F3F2",
        borderRadius: 20,
    },
    endRideContainer2: {
        marginVertical: 1,
        marginHorizontal: 1,
        backgroundColor: "#323232",
        flex: 1,
        alignItems: 'center',
    },
    endRideText: {
        fontSize: 40,
        textAlign: "center",
        color: "rgb(59,216,13)",
        marginVertical: 5,
    },
    statsContainer: {
        backgroundColor: '#F3F3F2',
        borderRadius: 15,
    },
    statsContainer2: {
        margin: 1,
        backgroundColor: "#323232",
    },
    statsContainer3: {
        marginHorizontal: 1,
        marginBottom: 1,
        backgroundColor: "#323232",
    },
    rideViewContainer: {
        marginHorizontal: 20,
        marginTop: 10,
        backgroundColor: "#323232",
    },
    touchContainer: {
        flex: 1,
        width: '100%',
        marginTop: 530,
        backgroundColor: "transparent",
    },
    rentText: {
        color: "rgb(59,216,13)",
    },
});
