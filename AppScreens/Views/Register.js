import React from 'react';
import { Text, View, TextInput, Button, Alert, StyleSheet, PermissionsAndroid, ImageBackground, Image } from 'react-native';
import firebase from 'react-native-firebase';
import * as EmailValidator from 'email-validator';
import { CheckBox } from 'react-native-elements'
import { TouchableOpacity } from 'react-native-gesture-handler';

const users = firebase.firestore().collection('users');

export class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            confirmPassword: '',
            name: '',
            agree: false,
        }
    }

    static navigationOptions = {
        header: null,
    }

    async requestLocationPermision() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: 'Esti de acord sa iti folosim locatia?',
                    message:
                        'Scooty are nevoie sa iti acceseze locatia ' +
                        'ca sa iti fie mai usor sa gasesti scootere in jurul tau',
                    buttonNeutral: 'Intraba-ma mai tarziu',
                    buttonNegative: 'Refuz',
                    buttonPositive: 'Accept',
                },
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('You can use the camera');
            } else {
                console.log('Camera permission denied');
            }
        }
        catch (error) {
            console.error(error);
        }
    }

    createAccount = () => {
        this.requestLocationPermision();
        if (this.state.name === '' || this.state.email === '' || this.state.password === '' || this.state.confirmPassword === '') {
            Alert.alert("Completeaza toate spatiile");
            return;
        }
        else if (this.state.confirmPassword !== this.state.password) {
            Alert.alert("Parolele nu coincid");
            return;
        }
        else if (EmailValidator.validate(this.state.email) === false) {
            Alert.alert("Email-ul nu este valid");
            return;
        }
        else {
            firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
                .then(response => {
                    const finalObj = {
                        email: this.state.email,
                        hasScooter: false,
                        hasCard: false,
                        name: this.state.name,
                    }
                    users.add(finalObj)
                        .then(() => {
                            this.props.navigation.navigate('MapsRT');
                        })
                        .catch(error => console.log(error));
                })
                .catch(error => console.log(error));
        }
    }

    navigateToLogin = () => {
        this.props.navigation.navigate('LoginRT');
    }

    render() {
        return (
            <ImageBackground style={{flex:1}} source={require('./images/Cluj7.jpg')}>
                <Text style={styles.text}>Creeaza un cont nou</Text>
                <View style={styles.Inputs}>
                    <View style={{flexDirection:"row",backgroundColor:"transparent"}}>
                    <Image source={require('./images/user6.png')} style={{width:45,height:45,opacity:0.75,marginRight:10}}/>
                    <TextInput
                        style={styles.input}
                        placeholder="Introdu numele tau..."
                        placeholderTextColor="white"
                        onChangeText={(text) => this.setState({ name: text })}
                        value={this.state.name}
                    />
                    </View>
                    <View style={{backgroundColor:"white",height:1,opacity:0.5,left:'18%',width:'82%'}}></View>
                    <View style={{flexDirection:"row",backgroundColor:"transparent"}}>
                    <Image source={require('./images/email.png')} style={{width:45,height:45,opacity:0.75,marginRight:10}}/>
                    <TextInput
                        style={styles.input}
                        placeholder="Introdu email-ul tau..."
                        keyboardType="email-address"
                        placeholderTextColor="white"
                        onChangeText={(text) => this.setState({ email: text })}
                        value={this.state.email}
                    />
                    </View>
                    <View style={{backgroundColor:"white",height:1,opacity:0.5,left:'18%',width:'82%'}}></View>
                    <View style={{flexDirection:"row",backgroundColor:"transparent"}}>
                    <Image source={require('./images/password2.jpg')} style={{width:45,height:45,opacity:0.75,marginRight:10}}/>
                    <TextInput
                        style={styles.input}
                        placeholder="Introdu parola..."
                        placeholderTextColor="white"
                        secureTextEntry={true}
                        onChangeText={(text) => this.setState({ password: text })}
                        value={this.state.password}
                    />
                    </View>
                    <View style={{backgroundColor:"white",height:1,opacity:0.5,left:'18%',width:'82%'}}></View>
                    <View style={{flexDirection:"row",backgroundColor:"transparent"}}>
                    <Image source={require('./images/password2.jpg')} style={{width:45,height:45,opacity:0.75,marginRight:10}}/>
                    <TextInput
                        style={styles.input}
                        placeholder="Confirma parola..."
                        placeholderTextColor="white"
                        secureTextEntry={true}
                        onChangeText={(text) => this.setState({ confirmPassword: text })}
                        value={this.state.confirmPassword}
                    />
                    </View>
                    <View style={{backgroundColor:"white",height:1,opacity:0.5,left:'18%',width:'82%'}}></View>
                </View>
                <View style={styles.Checkbox}>
                    <CheckBox
                        title="Sunt de acord cu termenii si conditiile"
                        checked={this.state.agree}
                        onPress={() => this.setState({ agree: !this.state.agree })}
                        containerStyle={{
                            backgroundColor: 'rgba(43,98,35,0)',
                            borderColor: 'rgba(43,98,35,0)'
                        }}
                        textStyle={{
                            color: 'white',
                            opacity: 0.75,
                            fontSize: 16,
                        }}
                    />
                </View>
                <View style={styles.RegisterButton}>
                    <Button
                        title="Creeaza contul"
                        onPress={this.createAccount}
                        color="rgba(46, 110, 37,0.9)"
                    />
                </View>
                <View style={styles.AlreadymemberButton}>
                    <Button
                        title="Ai deja un cont? Logheaza-te!"
                        onPress={this.navigateToLogin}
                        color="rgba(46, 110, 37,0.9)"
                    />
                </View>
                </ImageBackground>
        )
    }
}


const styles = StyleSheet.create({
    text: {
        top: '12%',
        left: '5%',
        //   width: 340,
        //   height: 90,
        color: "white",
        opacity:0.75,
        position: "absolute",
        fontSize: 34
    },

    Inputs: {
        top: '30%',
        marginHorizontal: '5%',
        backgroundColor: "transparent",
    },
    input: {
        backgroundColor: "transparent",
        opacity:0.75,
        fontSize:20,
        color:'white',
    },
    RegisterButton: {
        top: '70%',
        left: '5%',
        width: '90%',
        height: 50,
        position: "absolute"
    },
    Checkbox: {
        top: '60%',
        marginHorizontal:'5%',
        position: "absolute",
    },
    AlreadymemberButton: {
        top:'90%',
        left:'5%',
        width:'90%',
        height: 36,
        position: "absolute"
    }
});