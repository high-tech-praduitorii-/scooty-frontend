import React from 'react';
import { Text, TextInput, Button, StyleSheet, View, Alert, Image, ImageBackground,Icon } from 'react-native';
import firebase from 'react-native-firebase';
import { TouchableOpacity } from 'react-native-gesture-handler';

export class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    }
  }

  static navigationOptions = {
    header: null,
  }


  login = () => {
    if (this.state.email !== '' && this.state.password !== '') {
      firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
        .then(() => {
          this.props.navigation.navigate('MapsRT');
        })
        .catch(error => console.log(error));
    }
    else {
      Alert.alert("Introdu te rog toate datele");
    }
  }

  navigateToRegister = () => {
    this.props.navigation.navigate('RegisterRT');
  }

  navigateToForgotPassword = () => {

  }

  render() {
    return (
      <ImageBackground style={{flex:1}} source={require('./images/Cluj7.jpg')}>
        <Image  style={{width:'80%',height:'16%',marginTop:'20%',marginHorizontal:'10%',marginBottom:'50%',opacity:0.9}} source={require('./images/Logo_alb3.png')}></Image>
        <View style={{marginHorizontal:'10%', marginBottom:'10%'}}>
        <View style={{ backgroundColor:"rgba(255,255,255,0)"}}>
        <View style={{backgroundColor:"transparent",flexDirection:"row"}}>
        <Image source={require('./images/user6.png')} style={{width:45,height:45,opacity:0.75,marginRight:10}}></Image>
        <TextInput
            style={styles.UsernameInput}
            placeholder="Introdu email-ul tau..."
            placeholderTextColor="white"
            keyboardType="email-address"
            onChangeText={(text) => this.setState({ email: text })}
            value={this.state.email}
            autoCapitalize = "none"
          />
          </View>
          <View style={{backgroundColor:"white",height:1,opacity:0.5, left:'20%',width:'80%'}}></View>
         <View style={{backgroundColor:"transparent",flexDirection:"row"}}>
          <Image source={require('./images/password2.jpg')} style={{width:45,height:45,opacity:0.75,marginRight:10}}></Image>
          <TextInput
            style={styles.PasswordInput}
            placeholder="Introdu parola ta..."
            placeholderTextColor="white"
            secureTextEntry={true}
            onChangeText={(text) => this.setState({ password: text })}
            value={this.state.password}
          />
          </View>
          <View style={{backgroundColor:"white",height:1,opacity:0.5, left:'20%',width:'80%'}}></View>
          </View>
          </View>
          <View style = {styles.loginButton}>
            <Button title="Login" color="rgba(46, 110, 37,0.9)" onPress={() => this.login()} />
          </View>
          <View style={styles.registrationButton}>
          <Button title="Nu ai cont? Inregistreaza-te!" color="rgba(46, 110, 37,0.9)"  onPress={this.navigateToRegister} />
        </View>
      </ImageBackground>
    )
  }
}


const styles = StyleSheet.create({
  UsernameInput: {
    //width: '90%',
    backgroundColor: "transparent",
    color:'white',
    opacity:0.75,
    fontSize:20,
  },
  PasswordInput: {
    //width: '90%',
    backgroundColor: "transparent",
    color:'white',
    opacity:0.75,
    fontSize:20,
  },
  loginButton: {
    marginHorizontal: '10%',
  },
  registrationButton: {
    top: '90%',
    left:'10%',
    right:'10%',
    height: 36,
    position: "absolute",
    borderRadius:20,
  }
});