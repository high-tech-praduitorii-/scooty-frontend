import React from 'react';
import { Text, View, StyleSheet, TextInput } from 'react-native';
import QRCode from 'react-native-qrcode';
import { Button } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import firebase from 'react-native-firebase';

export class GenerateQR extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            showQR: false,
        }
    }

    static navigationOptions = ({ navigation }) => {
        return {
            header: null,
        }
    }

    changeQRVisibility = () => {
        this.setState({ showQR: !this.state.showQR });
    }

    showQR = () => {
        if (this.state.showQR === true) {
            return (
                <View>
                    <QRCode
                        value={this.state.text}
                        size={200}
                        bgColor='black'
                        fgColor='white' />
                    <Button title = "Ascunde codul QR" onPress = {this.changeQRVisibility} />
                </View>
            )
        }
    }

    showInputText = () => {
        if (this.state.showQR === false) {
            return (
                <View>
                    <TextInput
                        // style={styles.input}
                        placeholder="Scrie link-ul de la Raspberry PI"
                        onChangeText={(text) => this.setState({ text: text })}
                        value={this.state.text}
                    />
                    {/* <TouchableOpacity onPress={this.changeQRVisibility}>
                        <Text style = {{ fontWeight: 'bold' }}>Arata codul QR</Text>
                    </TouchableOpacity> */}
                    <Button title = "Arata codul QR" onPress = {this.changeQRVisibility} />
                </View>
            )
        }
    }


    addScooter = () => {
        const scooters = firebase.firestore().collection('scooters');
        scooters.add({
            QRID: this.state.text,
            isRented: false,
            location: {
                latitude: 46.770439,
                longitude: 23.591423
            }
        })
            .then(() => {
                this.props.navigation.navigate('MapsRT');
            })
            .catch(error => console.log(error));
    }

    render() {
        return (
            <View style={styles.container}>

                {this.showInputText()}
                {this.showQR()}
                <Button title = "Adauga scooterul" onPress = {this.addScooter} />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },

    input: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        margin: 10,
        borderRadius: 5,
        padding: 5,
    }
});