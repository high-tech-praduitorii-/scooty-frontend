import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Linking, Alert } from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import firebase from 'react-native-firebase';

export class ScanQR extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Scaneaza codul QR!',
            headerStyle: {
                backgroundColor: "#323232",
            },
            headerTitleStyle: {
                color: "rgb(59,216,13)"
            },
            headerTintColor: "rgb(59,216,13)"
        }
    }

    onSuccess = (e) => {
        this.requestScooterUnlock(e.data);
    }

    getCurrentUser = async () => {
        const users = firebase.firestore().collection('users');
        const currentUser = firebase.auth().currentUser.email;
        var userID = '';
        await users.get()
            .then(response => {
                response.docs.forEach(doc => {
                    const item = doc.data();
                    if (item.email === currentUser) userID = doc.id;
                })
            })
            .catch(error => console.log(error));
        return userID;
    }

    registerScooterToUser = (scooterID) => {
        this.getCurrentUser()
            .then(response => {
                const user = firebase.firestore().collection('users').doc(response);
                user.update({ hasScooter: true, scooter: scooterID });
            })
            .catch(error => console.log(error));
    }

    requestScooterUnlock = (link) => {
        // fetch(link)
        //     .then(r => r.json())
        //     .then((response) => {
        //         const scooters = firebase.firestore().collection('scooters');
        //         var scooter = {}, scooterID = '';
        //         scooters.get()
        //             .then(res => {
        //                 res.forEach(doc => {
        //                     const item = doc.data();
        //                     if (item.QRID.localeCompare(response)) {
        //                         scooter = item;
        //                         scooterID = doc.id;
        //                     }
        //                 })
        //             })
        //             .then(() => {
        //                 scooter.isRented = true;
        //                 scooters.doc(scooterID).set(scooter);
        //             })
        //             .catch(error => console.log(error));
        //     })
        //     .catch(error => {
        //         console.log(error);
        //     })
        
        fetch(link)
            .then(res => res.text())
            .then(response => {
                var scooterID = '';
                const scooters = firebase.firestore().collection('scooters');
                scooters.get()
                    .then(resp => {
                        resp.forEach(doc => {
                            const item = doc.data();
                            if (item.QRID.localeCompare(response) === 0) {
                                scooterID = doc.id;
                            }
                        })
                    })
                    .then(() => {
                        scooters.doc(scooterID).update({ isRented: true })
                            .then(() => {
                                this.registerScooterToUser(scooterID);
                                this.props.navigation.goBack();
                            })
                            .catch(error => console.log(error));
                    })
                    .catch(error => console.log(error));
            })
            .catch(error => console.log(error));
    }

    navigateToGenerateQR = () => {
        this.props.navigation.navigate('GenerateQRRT');
    }

    render() {
        return (

            <QRCodeScanner
                onRead={this.onSuccess}
                // topContent={
                //     <Text style={styles.centerText}>Scaneaza codul QR de pe scooter pentru a-l inchiria!</Text>
                // }
                // bottomContent = {
                //     <TouchableOpacity onPress = {this.navigateToGenerateQR}>
                //         <Text>Adauga un scooter</Text>
                //     </TouchableOpacity>
                // }
                cameraStyle={{ height: '100%' }}
            />

        )
    }
}

const styles = StyleSheet.create({
    centerText: {
        flex: 1,
        fontSize: 18,
        padding: '5%',
        color: '#777',
    },
    textBold: {
        fontWeight: '500',
        color: '#000',
    },
    buttonText: {
        fontSize: 21,
        color: 'rgb(0,122,255)',
    },
    buttonTouchable: {
        padding: 16,
    },
});