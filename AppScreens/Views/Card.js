import React from 'react';
import firebase from 'react-native-firebase';
import {ImageBackground, View, Button, TextInput,Text, StyleSheet, Image} from 'react-native';
import { NativeEventsReceiver } from 'react-native-navigation/lib/dist/adapters/NativeEventsReceiver';

export class Card extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            name: '',
            cardNumber: '',
            month: '',
            year: '',
            cvv: '',
        }
    }

    static navigationOptions = ({ navigation }) => {
        return {
            headerStyle: {
                backgroundColor: "#323232",
            },
            headerTintColor: "rgb(59,216,13)"
        }
    }

    navigateToScanQR= () =>{
        this.props.navigation.navigate('ScanQR');
    }

    addCard= () =>{
        this.registerCardToUser();
        this.navigateToScanQR();
    }

    getCurrentUser = async () => {
        const users = firebase.firestore().collection('users');
        const currentUser = firebase.auth().currentUser.email;
        var userID = '';
        await users.get()
          .then(response => {
            response.docs.forEach(doc => {
              const item = doc.data();
              if (item.email === currentUser) userID = doc.id;
            })
          })
          .catch(error => console.log(error));
        return userID;
      }

    registerCardToUser = () => {
        this.getCurrentUser()
            .then(response => {
                const user = firebase.firestore().collection('users').doc(response);
                user.update({ hasCard: true });
            })
            .catch(error => console.log(error));
    }

    render(){
        return(
            <ImageBackground style={{flex:1}} source={require('./images/Cluj7.jpg')}>
                <Text style={{paddingTop:'15%',paddingLeft:'5%',color:"white",fontSize:30,opacity:0.9,marginBottom:'25%'}}>Adauga detalile cardului</Text>
                <View style={{flexDirection:"row",marginHorizontal:'5%',backgroundColor:"transparent"}}>
                    <Image style={{width:45,height:45}} source={require('./images/user6.png')}></Image>
                    <TextInput
                        style={styles.input}
                        placeholder="Introdu numele de pe card..."
                        placeholderTextColor="white"
                        onChangeText={(text) => this.setState({ name: text })}
                        value={this.state.name}
                    />
                </View>
                <View style={{backgroundColor:"white",height:1,opacity:0.5,left:'22%',width:'73%'}}></View>
                <View style={{flexDirection:"row",marginHorizontal:'5%',backgroundColor:"transparent"}}>
                    <Image style={{width:45,height:45}} source={require('./images/card3.png')}></Image>
                    <TextInput
                        style={styles.input}
                        placeholder="Introdu numarul cardului..."
                        keyboardType="decimal-pad"
                        placeholderTextColor="white"
                        maxLength={16}
                        onChangeText={(text) => this.setState({ cardNumber: text })}
                        value={this.state.cardNumber}
                    />
                </View>
                <View style={{backgroundColor:"white",height:1,opacity:0.5,left:'22%',width:'73%'}}></View>
                <View style={{flexDirection:"row",marginHorizontal:'5%',backgroundColor:"transparent"}}>
                    <Image style={{width:45,height:45}} source={require('./images/date.jpg')}></Image>
                    <TextInput
                        style={styles.input}
                        placeholder="mm"
                        keyboardType="decimal-pad"
                        placeholderTextColor="white"
                        maxLength={2}
                        autoCompleteType='cc-exp-month'
                        onChangeText={(text) => this.setState({ month: text })}
                        value={this.state.month}
                    />
                      <TextInput
                        style={styles.input}
                        placeholder="yyyy"
                        keyboardType="decimal-pad"
                        placeholderTextColor="white"
                        maxLength={4}
                        autoCompleteType='cc-exp-year'
                        onChangeText={(text) => this.setState({ year: text })}
                        value={this.state.year}
                    />
                    </View>
                    <View styles={{flexDirection:"row"}}>
                        <View style={{backgroundColor:"white",height:1,opacity:0.5,left:'22%',width:'13%'}}></View>
                        <View style={{backgroundColor:"white",height:1,opacity:0.5,left:'38%',width:'13%'}}></View>
                    </View>
                    <View style={{flexDirection:"row",marginHorizontal:'5%',backgroundColor:"transparent"}}style={{flexDirection:"row",marginHorizontal:'5%',backgroundColor:"transparent"}}>
                    <Image style={{width:45,height:45}} source={require('./images/cvv.png')}/>
                      <TextInput
                        style={styles.input}
                        placeholder="CVV"
                        keyboardType="decimal-pad"
                        placeholderTextColor="white"
                        maxLength={3}
                        onChangeText={(text) => this.setState({cvv : text })}
                        value={this.state.cvv}
                    />
                </View>
                <View style={{backgroundColor:"white",height:1,opacity:0.5,left:'22%',width:'13%'}}></View>
                <View style={{marginHorizontal:'5%',marginTop:'10%', color:"transparent"}}>
                    <Button title="Adauga cardul" color="rgba(46, 110, 37,0.9)" onPress={this.addCard}/>
                </View>
            </ImageBackground>
        )
    }
    }
    const styles=StyleSheet.create({
        container: {
            backgroundColor:"white",
            marginHorizontal:'10%',
            top:'20%',
        },
        input: {
            backgroundColor: "transparent",
            opacity:0.75,
            fontSize:20,
            marginLeft:'5%',
            color:'white',
        },
    });