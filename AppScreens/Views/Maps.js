import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Platform,
  PermissionsAndroid,
  Animated,
  Easing
} from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker, AnimatedRegion, Polyline } from 'react-native-maps';
import { Button, Icon } from 'react-native-elements';
import firebase from 'react-native-firebase';
import haversine from 'haversine';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import MapViewDirections from 'react-native-maps-directions';
import RNGooglePlaces from 'react-native-google-places';
import RideView from '../Sections/RideView.js'

var LATITUDE = 37.78825;
var LONGITUDE = 25.4324;
const LATITUDE_DELTA = 0.009;
const LONGITUDE_DELTA = 0.009;
const GOOGLE_API_KEY = 'AIzaSyD-u2hMRh2jKOzAOvTqaRNhPiipcnutDhE';

export class Maps extends Component {
  constructor(props) {
    super(props);
    this.state = {
      latitude: LATITUDE,
      longitude: LONGITUDE,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
      coords: null,
      routeCoordinates: [],
      distanceTravelled: 0,
      prevLatLng: {},
      coordinate: new AnimatedRegion({
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: 0,
        longitudeDelta: 0
      }),
      loading: true,
      // timer: null,
      rideStarted: false,
      scooters: [],
      inRide: false,
      userID: '',
      searchText: '',
      origin: null,
      destination: null,
      scooterID: '',
      show: true,
    }
    this.unsubscribe = null;
    this.user = null;
    this.animatedValue = new Animated.Value(0);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      header: null,
    }
  }

  animate() {
    const toVal = this.state.show ? 1 : 0;
    Animated.timing(
      this.animatedValue,
      {
        toValue: toVal,
        duration: 200,
        easing: Easing.linear
      }
    ).start()
  }


  updateMapRegion = (region) => {
    // this.setState({
    //   coords: region,
    //   latiduteDelta: region.latitudeDelta,
    //   longitudeDelta: region.longitudeDelta
    // })
    this.state.coords = {
      latitude: region.latitude,
      longitude: region.longitude,
    };
    this.state.longitudeDelta = region.longitudeDelta;
    this.state.latitudeDelta = region.latitudeDelta;
  }

  checkIfExists = (id) => {
    var exists = false;
    this.state.scooters.map(scooter => {
      if (scooter.id === id) exists = true;
    })
    return exists;
  }

  deleteElement = (id) => {
    var index = 0;
    this.state.scooters.map((scooter, idx) => {
      if (scooter.id === id) index = idx;
    });
    this.state.scooters.splice(index, 1);
    this.setState({ scooters: this.state.scooters });
  }

  getCurrentUser = async () => {
    const users = firebase.firestore().collection('users');
    const currentUser = firebase.auth().currentUser.email;
    var userID = '';
    await users.get()
      .then(response => {
        response.docs.forEach(doc => {
          const item = doc.data();
          if (item.email === currentUser) userID = doc.id;
        })
      })
      .catch(error => console.log(error));
    this.setState({ userID: userID });
    return userID;
  }

  inRideListener = (id) => {
    const user = firebase.firestore().collection('users').doc(id);
    this.user = user.onSnapshot(doc => {
      const item = doc.data();
      if (item.hasScooter === true) {
        this.setState({ inRide: true, scooterID: item.scooter });
      }
      else {
        this.setState({ inRide: false });
      }
    })
  }

  hasCardAdded = async () => {
    const user = firebase.firestore().collection('users').doc(this.state.userID);
    var creditCard = false;
    await user.get()
      .then(doc => {
        const item = doc.data();
        if (item.hasCard === true)
          creditCard = true;
      })
      .catch(error => console.log(error))
    return creditCard;
  }

  onCollectionUpdate = (querrySnapshot) => {
    querrySnapshot.forEach(doc => {
      const item = doc.data();
      if (item.isRented === true) {
        if (this.checkIfExists(doc.id) === true) this.deleteElement(doc.id);
      }
      else {
        if (this.checkIfExists(doc.id) === true && item.isRented === false) {
          this.state.scooters.map(scooter => {
            if (scooter.id === doc.id) {
              scooter.latitude = item.location.latitude;
              scooter.longitude = item.location.longitude;
            }
          })
        }
        else if (item.isRented === false) {
          this.state.scooters.push({
            latitude: item.location.latitude,
            longitude: item.location.longitude,
            id: doc.id
          });
        }
      }
    })
    this.setState({ scooters: this.state.scooters });
  }

  getScooterCoords = () => {
    const scooters = firebase.firestore().collection('scooters');
    this.unsubscribe = scooters.onSnapshot(this.onCollectionUpdate);
  }

  componentDidMount() {
    this.animate();
    this.getScooterCoords();
    this.getCurrentUser()
      .then(response => {
        this.inRideListener(response);
        this.setState({ userID: response });
      })
      .catch(error => console.log(error));
    // this.inRideListener(userID);
    // this.interval = setInterval(() => {
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({ interval: 10000, fastInterval: 5000 })
      .then(data => {
        navigator.geolocation.getCurrentPosition(
          position => {
            const { coordinate, routeCoordinates, distanceTravelled } = this.state;
            const { latitude, longitude } = position.coords;
            const newCoordinate = {
              latitude,
              longitude
            };
            if (this.state.latitude !== latitude || this.state.longitude !== longitude) {
              this.setState({
                latitude,
                longitude,
                routeCoordinates: routeCoordinates.concat([newCoordinate]),
                distanceTravelled:
                  distanceTravelled + this.calcDistance(newCoordinate),
                prevLatLng: newCoordinate,
                coords: {
                  latitude: position.coords.latitude,
                  longitude: position.coords.longitude,
                },
                loading: false,
              });
            }
          },
          error => {
            console.log(error)
            this.setState({
              latitude: LATITUDE,
              longitude: LONGITUDE,
              longitudeDelta: 50,
              latitudeDelta: 50,
              loading: false,
            })
          },
          { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
      })
      .catch(error => console.log(error));
    // }, 100)
  }

  componentWillUnmount() {
    this.unsubscribe();
    this.user();
  }

  calcDistance = newLatLng => {
    const { prevLatLng } = this.state;
    return haversine(prevLatLng, newLatLng) || 0;
  };

  getMapRegion = () => ({
    latitude: this.state.coords !== null ? this.state.coords.latitude : this.state.latitude,
    longitude: this.state.coords !== null ? this.state.coords.longitude : this.state.longitude,
    latitudeDelta: this.state.latitudeDelta,
    longitudeDelta: this.state.longitudeDelta
  });

  loadingScreen = () => {
    return (<View></View>)
  }

  signOut = () => {
    firebase.auth().signOut()
      .then(() => {
        this.props.navigation.navigate('LoginRT');
      })
      .catch(error => console.error(error));
  }

  scanQR = () => {
    this.props.navigation.navigate('ScanQR');
  }

  onScooter = () => {
    return (true);
  }

  getScooterID = (qrID) => {
    const scooters = firebase.firestore().collection('scooters');
    var scooterID = '';
    scooters.get()
      .then(response => {
        response.docs.forEach(doc => {
          const item = doc.data();
          if (item.QRID === qrID) scooterID = doc.id;
        })
      })
      .catch(error => console.log(error));
    return scooterID;
  }


  endRide = (minutes, seconds, lei, bani) => {
    const user = firebase.firestore().collection('users').doc(this.state.userID);
    const rentals = firebase.firestore().collection('rentals');
    const scooter = firebase.firestore().collection('scooters').doc(this.state.scooterID);
    rentals.add({
      price: Number(lei) + Number(bani) / 100,
      time: Number(minutes) * 60 + Number(seconds),
      userID: this.state.userID,
      scooterID: this.state.scooterID
    });
    scooter.update({ isRented: false });
    user.update({ hasScooter: false })
      .then(() => {
        this.setState({
          inRide: false, timer: null, minutes_Counter: '00',
          seconds_Counter: '00',
          lei_Counter: '3',
          bani_Counter: '00',
          rideStarted: false,
        });
      })
      .catch(error => console.log(error));
  }

  searchLocation = () => {
    navigator.geolocation.getCurrentPosition(
      position => {
        const { latitude, longitude } = position.coords;
        this.setState({ origin: { latitude: latitude, longitude: longitude } });
      },
      error => {
        console.log(error);
        this.cancelDirections();
      },
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );

    RNGooglePlaces.openAutocompleteModal()
      .then(place => {
        this.setState({ destination: place.location });
      })
      .catch(error => {
        console.log(error);
        this.cancelDirections();
      });
  }

  renderRouteAfterSearch = () => {
    if (this.state.origin !== null && this.state.destination !== null) {
      return (
        <MapViewDirections
          origin={this.state.origin}
          destination={this.state.destination}
          apikey={GOOGLE_API_KEY}
          strokeWidth={3}
          strokeColor="black"
        />
      )
    }
    else return null;
  }

  cancelDirections = () => {
    this.setState({ origin: null, destination: null });
  }

  renderSearchOrQuitButton = () => {
    if (this.state.origin === null && this.state.destination === null) {
      return (
        <Icon name="search" color="rgb(59, 216, 13)" size={36} onPress={this.searchLocation} />
      )
    }
    else {
      return (
        <Icon name="cancel" color="rgb(59, 216, 13)" size={36} onPress={this.cancelDirections} />
      )
    }
  }

  goToScooter = (coords, position) => {
    const { latitude, longitude } = coords;
    navigator.geolocation.getCurrentPosition(
      position => {
        const { latitude, longitude } = position.coords;
        this.setState({
          origin: {
            latitude: latitude,
            longitude: longitude
          }
        });
      }
    )
    this.setState({
      destination: {
        latitude: latitude,
        longitude: longitude
      }
    });
  }

  startRide = () => {
    this.interval = setInterval(() => {

      var num = (Number(this.state.seconds_Counter) + 1).toString(),
        count = this.state.minutes_Counter;
      bani = this.state.bani_Counter;
      lei = this.state.lei_Counter;

      if (Number(this.state.seconds_Counter) == 59) {
        count = (Number(this.state.minutes_Counter) + 1).toString();
        num = '00';
        bani = (Number(this.state.bani_Counter) + 25).toString();
      }

      if (Number(bani) == 100) {
        lei = (Number(this.state.lei_Counter) + 1).toString();
        bani = '00';
      }

      this.setState({
        minutes_Counter: count.length == 1 ? '0' + count : count,
        seconds_Counter: num.length == 1 ? '0' + num : num,
        bani_Counter: bani,
        lei_Counter: lei
      });
    }, 1000);
    this.setState({ timer: this.interval });
  }

  navigateToCard = () => {
    this.props.navigation.navigate('CardRT');
  }

  render() {
    const opacity = this.animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 0.5, 1]
    })
    if (this.state.loading === true) return this.loadingScreen();
    else if (this.state.inRide) {
      return (
        <View style={styles.container}>
          <MapView
            provider={PROVIDER_GOOGLE}
            style={styles.map}
            region={this.getMapRegion()}
            showsUserLocation={true}
            followsUserLocation={true}
            loadingEnabled={true}
            onRegionChange={this.updateMapRegion}
            onPress={() => {
              this.setState({ show: !this.state.show });
              this.animate();
            }}
          >
            {
              this.state.scooters.map(scooter => (
                <Marker coordinate={{
                  latitude: scooter.latitude,
                  longitude: scooter.longitude
                }} title="Trotineta disponibila"
                  key={scooter.id}
                  onPress={() => this.goToScooter({ latitude: scooter.latitude, longitude: scooter.longitude })} />
              ))
            }
            {this.renderRouteAfterSearch()}
          </MapView>
          {/* <View style={styles.headerContainer}>
            {this.renderSearchOrQuitButton()}
            <View style={styles.statsTitleContainer}>
              <Text style={styles.statsTitleText}>DETALIILE CURSEI</Text>
            </View>
            <Icon name='exit-to-app' color="rgb(59,216,13)" size={36} onPress={this.signOut} />
          </View>
          <View style={styles.rideView}>
            <RideView endRide={this.endRide} />
          </View> */}
          <Animated.View style={[styles.headerContainer, { opacity: opacity }]}>
            {this.renderSearchOrQuitButton()}
            <View style={styles.statsTitleContainer}>
              <Text style={styles.statsTitleText}>DETALIILE CURSEI</Text>
            </View>
            <Icon name='exit-to-app' color="rgb(59,216,13)" size={36} onPress={this.signOut} />
          </Animated.View>
          <Animated.View style={[styles.rideView, { opacity: opacity }]}>
            <RideView endRide={this.endRide} />
          </Animated.View>
        </View>
      );
    }
    else
      return (
        <View style={styles.container}>
          <MapView
            provider={PROVIDER_GOOGLE}
            style={styles.map}
            region={this.getMapRegion()}
            showsUserLocation={true}
            followsUserLocation={true}
            loadingEnabled={true}
            onRegionChangeComplete={this.updateMapRegion}
            onPress={() => {
              this.setState({ show: !this.state.show });
              this.animate();
            }}
          >
            {
              this.state.scooters.map(scooter => (
                <Marker coordinate={{
                  latitude: scooter.latitude,
                  longitude: scooter.longitude
                }} title="Trotineta disponibila"
                  key={scooter.id}
                  onPress={() => this.goToScooter({ latitude: scooter.latitude, longitude: scooter.longitude })} />
              ))
            }
            {this.renderRouteAfterSearch()}
          </MapView>
          <Animated.View style={[styles.headerContainer, { opacity: opacity }]}>
            {this.renderSearchOrQuitButton()}
            <View style={styles.statsTitleContainer}>
              <Text style={styles.statsTitleText}>WezX</Text>
            </View>
            <Icon name='exit-to-app' color="rgb(59,216,13)" size={36} onPress={this.signOut} />
          </Animated.View>
          <Animated.View style={[styles.touchContainer, { opacity: opacity }]}>
            <View style={styles.buttonContainer}>
              <TouchableOpacity style={[styles.bubble, styles.button]} onPress={() => {
                this.hasCardAdded()
                  .then(response => {
                    if (response)
                      this.scanQR();
                    else
                      this.navigateToCard();
                  })
              }}>
                <Text style={styles.rentText}>
                  Inchiriaza o trotineta!
              </Text>
              </TouchableOpacity>
            </View>
          </Animated.View>
        </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  hidden: {
    opacity: 0
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: "center",
    marginHorizontal: 10
  },
  buttonContainer: {
    flexDirection: "row",
    // marginBottom: 20,
    backgroundColor: "transparent",
    top: '20%'
  },
  bubble: {
    flex: 1,
    backgroundColor: "rgba(32,32,32,0.65)",
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
    // bottom: 10
  },
  latlng: {
    width: 200,
    alignItems: "stretch"
  },
  rideView: {
    flex: 1,
    marginTop: 500,
    width: '100%',
    backgroundColor: "#323232",
  },
  headerContainer: {
    flexDirection: "row",
    height: '10%',
    alignItems: "center",
    backgroundColor: "#323232",
  },
  statsText: {
    fontSize: 28,
    textAlign: "center",
    color: "rgb(59,216,13)",
    marginVertical: 5,
  },
  statsTitleContainer: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#323232",
  },
  statsTitleText: {
    textAlign: "center",
    fontSize: 26,
    color: "rgb(59,216,13)",
  },
  endRideContainer: {
    flex: 1,
    marginHorizontal: 20,
    marginVertical: 10,
    backgroundColor: "#F3F3F2",
    borderRadius: 20,
  },
  endRideContainer2: {
    marginVertical: 1,
    marginHorizontal: 1,
    backgroundColor: "#323232",
    flex: 1,
    alignItems: 'center',
  },
  endRideText: {
    fontSize: 40,
    textAlign: "center",
    color: "rgb(59,216,13)",
    marginVertical: 5,
  },
  statsContainer: {
    backgroundColor: '#F3F3F2',
    borderRadius: 15,
  },
  statsContainer2: {
    margin: 1,
    backgroundColor: "#323232",
  },
  statsContainer3: {
    marginHorizontal: 1,
    marginBottom: 1,
    backgroundColor: "#323232",
  },
  rideViewContainer: {
    marginHorizontal: 20,
    marginTop: 10,
    backgroundColor: "#323232",
  },
  touchContainer: {
    flex: 1,
    width: '100%',
    marginTop: 530,
    backgroundColor: "transparent",
  },
  rentText: {
    color: "rgb(59,216,13)",
  },
});
